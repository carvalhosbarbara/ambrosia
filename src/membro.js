class Membro {
    
    constructor(id, equipe, cpf, nome) {
        
        this._id = id;
        this._equipe = equipe;
        this._cpf = cpf;
        this._nome = nome;
        Object.freeze(this);
    }
    
    get id() {
        
        return this._id;
    }

    get equipe() {
        
        return this._equipe;
    }
    
    get cpf() {
        
        return this._cpf;
    }
    get nome() {
        
        return this._nome;
    }

}