
---

## AMBROSIA - uma solução para Gerenciamento de Tarefas

O sistema Ambrosia foi criado para proporcionar maior facilidade no gerenciamento de tarefas para o Gerente de Projetos. Com esse projeto, no perfil de Gerente, você pode criar novas tarefas antes de atribuí-la a um membro de equipe, criar membros de equipe, criar novos gerentes e novas equipes. 

As tarefas de cada equipe são mostradas em um gráfico de Gantt, que permite uma melhor visualização do andamento de todas as tarefas. 

O gerente também pode acompanhar os status das tarefas por meio de um gráfico na página inicial. 

O perfil de Membro de Equipe visualiza suas tarefas na página inicial. Se a tarefa tiver algum impedimento, é possível reportar esse impedimento ao gerente responsável pela tarefa. 

O gerente visualiza os impedimentos na página inicial e pode alterá-lo e marcar como concluído.


---

## Como rodar o projeto 

Para rodar o projeto, basta clonar o repositório e dentro da pasta ambrosia executar o comando:
**node app.js**
e pronto o servidor está rodando. 
