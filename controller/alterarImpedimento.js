$(document).ready(function(){
                                const url = 'http://localhost:3000/alterarImpedimento';
                                $('#alterar').click(function(){
                                    
                                  function null_or_empty(str) {
                                    var v = document.getElementById(str).value;
                                    return v == null || v == "";
                                }
                                
                                function valida_form() {
                                    if (null_or_empty("titulo")
                                            || null_or_empty("numero")
                                            || null_or_empty("descricao")
                                            || null_or_empty("calendario")
                                            || null_or_empty("status"))
                                    {
                                        alert('Preencha todos os campos');
                                        return false;
                                    }
                                    return true;
                                }

                                if(valida_form()){
                                  const titulo = $("#titulo").val();  
                                  const numero = $("#numero").val();
                                  const descricao = $("#descricao").val();
                                  const calendario = $("#calendario").val();
                                  const status = $("#status").val();

                                  vData = { titulo:titulo, descricao:descricao, calendario:calendario, numero:numero, status:status };
                                  $.ajax({
                                    url:url,
                                    type:"POST",
                                    data:vData,
                                    success: function(result){
                                      console.log('success')
                                      alert('Impedimento alterado com sucesso');
                                      window.location.href ='/Users/BarbaraSimoes/Desktop/Ambrosia/ambrosia/views/indexGerencial.html';
                                    },
                                    error: function(error){
                                      console.log(error.status)  
                                      
                                    }
                                  })
                                }
                                })
                              })  