 $(document).ready(function(){
                                const url = 'http://localhost:3000/Ambrosia/src/listarEquipes.js';
                                var usuario = JSON.parse(sessionStorage.getItem('usuario'));
                                vData = { usuario:usuario}
                                  $.ajax({
                                    url:url,
                                    type:"POST",
                                    data:vData,
                                    success: function(result){

                                        var response = result;
                                        var arraydearray = [];  

                                        $.each(response, function () {
                                            var array = [];
                                            $.each(this, function (k, v) {

                                              if (k == 'equi_nome' && v!= null){
                                                array.push(v)
                                              }
                                              if (k == 'equi_id' && v!= null){
                                                array.push(v)
                                              }
                                            }) 
                                            arraydearray.push(array)               
                                        })
                                        $(document).ready(function() {
                                          var nColNumber = -1; 
                                          var table = $('#table').DataTable( {
                                              data: arraydearray,
                                              columns: [
                                                  { 'targets': [ ++nColNumber ],title: "Número de identificação" },
                                                  { 'targets': [ ++nColNumber ],title: "Nome" }
                                              ],
                                              "language": {
                                                "sEmptyTable": "Nenhum registro encontrado",
                                                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                                                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                                                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                                                "sInfoPostFix": "",
                                                "sInfoThousands": ".",
                                                "sLengthMenu": "_MENU_ resultados por página",
                                                "sLoadingRecords": "Carregando...",
                                                "sProcessing": "Processando...",
                                                "sZeroRecords": "Nenhum registro encontrado",
                                                "sSearch": "Pesquisar",
                                                "oPaginate": {
                                                    "sNext": "Próximo",
                                                    "sPrevious": "Anterior",
                                                    "sFirst": "Primeiro",
                                                    "sLast": "Último"
                                                },
                                                "oAria": {
                                                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                                                    "sSortDescending": ": Ordenar colunas de forma descendente"
                                                },
                                                "select": {
                                                    "rows": {
                                                        "_": "Selecionado %d linhas",
                                                        "0": "Nenhuma linha selecionada",
                                                        "1": "Selecionado 1 linha"
                                                    }
                                                  }}
                                          } );
                                          
                                          $('#table').on('click', 'tr', function () {
                                              var data = table.row( this ).data();
                                              var dadosEquipe = JSON.stringify(data[0]);
                                              sessionStorage.setItem('chave', dadosEquipe );
                                              console.log('enviando dados' + dadosEquipe)  
                                              window.location.href ='/Users/BarbaraSimoes/Desktop/Ambrosia/ambrosia/views/alterarEquipe.html';
                                          } );
                                      } );
                                      console.log('success')
                                    },
                                    error: function(error){
                                      console.log(error.status)  
                                        alert("Erro ao buscar membros!");
                                      
                                      
                                      
                                    }
                                  })
                                }) 