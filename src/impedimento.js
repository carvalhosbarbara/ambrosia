class Impedimento {
    
    constructor(id, tarefa, data, mensagem) {
        
        this._id = id;
        this._tarefa = tarefa;
        this._data = new Date(data.getTime());
        this._mensagem = mensagem;
        Object.freeze(this);
    }
    
    get id() {
        
        return this._id;
    }

    get tarefa() {
        
        return this._tarefa;
    }
    
    get data() {
        
        return new Date(this._data.getTime());
    }
    get mensagem() {
        
        return this._mensagem;
    }

}