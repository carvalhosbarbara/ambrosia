const express = require('express')
, app = express()
, pool = require('./pool-factory')
, connectionMiddleware = require('./connection-middleware');

// ativando nosso middleware
app.use(connectionMiddleware(pool));

// registra as rotas
require('./src/tarefa')(app);

// middleware de tratamento de erro
app.use((err, req, res, next) => {
    console.error(err.stack);
	res.status(500).json({ error: err.toString() });
});

module.exports = app;